#include <iostream>
#include <chrono>
#include <random>
#include <algorithm>

#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

#define MAX_SIZE_INPUT 100
#define MAX_SIZE_OUTPUT 10000

#define MAX_DIFF 60.0f

// alows us to get random element from vector easily
template<typename Iter, typename RandomGenerator>
Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
    std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
    std::advance(start, dis(g));
    return start;
}

template<typename Iter>
Iter select_randomly(Iter start, Iter end) {
    static std::random_device rd;
    static std::mt19937 gen(rd());
    return select_randomly(start, end, gen);
}

// Resize the image if it is larger than given size
void resizeIfLarger( cv::Mat &img, const int &max_size)
{

        // if something is larger resize it preserving the correct ratios
        if (img.cols > max_size || img.rows > max_size)
        {
            float ratio = (float)img.cols / (float)img.rows;

            int new_width = (ratio > 1) ? max_size : (int)(max_size * ratio);
            int new_height = (ratio < 1) ? max_size : (int)((float)max_size / ratio);
            cv::resize( img, img, cv::Size(new_width, new_height));
        }
}

///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
//////                            MAIN FUNCTION                                      //////
///////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    auto start = std::chrono::steady_clock::now();

    if (argc < 2)
    {
        std::cerr << "Usage: DITHERED <PathToImage>";
        return 1;
    }

    const std::string path_folder_data = "./small_img/";
    const std::string path_input_img = argv[1];

    // Tries to load image
    cv::Mat in_img;
    in_img = cv::imread(path_input_img);
    if (in_img.empty())
    {
        std::cerr << "Couldn't load image, exited\n";
        return 1;
    }

    // Resize img if larger than given size
    resizeIfLarger(in_img, MAX_SIZE_INPUT);


    // First loop over the data and push everything in a big vector
    // Vector will contains images, other their mean rgbs
    std::vector<cv::Mat> data;
    std::vector<cv::Scalar> means;

    std::cout << "-- Loading the images in memory\n";

    // Get all jpg files in folderData
    std::vector<std::string> fileNames;
    cv::glob(path_folder_data + "*.jpg", fileNames);

    // Loop the files and pushes them
    for (const auto &file : fileNames)
    {
        cv::Mat img = cv::imread(file);
        cv::Scalar m = cv::mean(img);
        data.push_back(img);
        means.push_back(m);
    }

    // We assume all of the data images have the same size
    const int d_width = data[0].cols, d_height = data[0].rows;

    auto end = std::chrono::steady_clock::now();
    std::cout   << "-- " << data.size()
                << " images loaded into memory, done in "
                << std::chrono::duration_cast<std::chrono::seconds>(end - start).count() << " seconds\n";




    // Create canvas with somewhat plausible size
    cv::Mat canvas(cv::Size(d_width * in_img.cols, d_height * in_img.rows),
        CV_8UC3, cv::Scalar(255, 255, 255));

    std::cout << "-- Started rendering composition\n";

    start = std::chrono::steady_clock::now();

    // Loop over the pixels
    typedef cv::Point3_<u_int8_t> Pixel;
    
    // This will contain the errors
    std::vector<std::vector<cv::Scalar>> errors(in_img.rows,
            std::vector<cv::Scalar>(in_img.cols, cv::Scalar(.0f, .0f, .0f))
                                                );

    for (int j = 0; j < in_img.rows; j++)
    {
        for (int i = 0; i < in_img.cols; i++)
        {
            // Destination position
            int x = i * d_width, y = j * d_height;
            Pixel old_pixel = in_img.at<Pixel>(j,i);

            // Convert pixel to scalar and add the error to it
            cv::Scalar new_pixel = cv::Scalar(old_pixel.x, old_pixel.y, old_pixel.z);
            new_pixel += errors[j][i];
            


            // Find the closest image to this pixel

            // Will contain the distances
            std::vector<float> dist;
            dist.reserve(data.size());

            // Compute distance to each mean in our dataset
            std::for_each(means.begin(), means.end(), [&](const cv::Scalar &color) -> void {
                dist.emplace_back( cv::norm( color - new_pixel, cv::NORM_L2SQR));
            });


            // We want to pick the closest k elements so that we can vary a bit the resulting
            // image

            // We create an index array, sort the dist linked to this array, and the we just have the first
            // k argmin
            std::vector<int> idx(dist.size(), 0);
            for (std::size_t k = 0, max = idx.size(); k != max; k++)
            {
                idx[k] = k;
            }

            std::sort(idx.begin(), idx.end(), [&](const int &a, const int &b) {
                return dist.at(a) < dist.at(b);
            });

            // Select a random index in the first say 5
            // find first index where it is larger than given value
            auto low_bound = std::lower_bound( idx.begin(), idx.end(), MAX_DIFF,
                [&](const int &a, const float &b){
                    return dist[a] < b;
                });

            int choice = idx[0];
            if (low_bound != idx.begin())
            {
                choice = *select_randomly(idx.begin(), low_bound);
                //std::cout << std::distance(idx.begin(), low_bound) << '\n';
            }


            // Selects the correct area and put the image at position argmin there
            cv::Mat roi = canvas(cv::Rect(x, y, d_width, d_height));
            data[choice].copyTo(roi);

            // Get the error
            cv::Scalar err = means[choice];
            err[0] = old_pixel.x - err[0];
            err[1] = old_pixel.y - err[1];
            err[2] = old_pixel.z - err[2];

            // Propagate the error
            if (i < in_img.cols - 1)
                errors[j][i+1] += err * 7.0f / 16.0f;

            if (j < in_img.rows - 1)
            {
                if (i!=0)
                    errors[j+1][i-1] += err * 3.0f / 16.0f;

                if (i < in_img.cols - 1)
                    errors[j+1][i+1] += err * 1.0f / 16.0f;

                errors[j+1][i] += err * 5.0f / 16.0f;
            }

        }
    }

    end = std::chrono::steady_clock::now();
    std::cout   << "-- Done looping the pixels in "
                << std::chrono::duration_cast<std::chrono::seconds>(end-start).count() << " seconds\n";

    // Reduce output size
    resizeIfLarger(canvas, MAX_SIZE_OUTPUT);

    cv::imwrite("./OUTPUT.jpg", canvas);


    return 0;
}
